import cv2 as cv 
import numpy as np

canvasH, canvasW = 1000, 1414

def find_paper_corners(frame):
    frame_hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV) 
    frame_r, frame_c, _ = frame_hsv.shape

    # tunable params 
    sensitivity = 15
    GREEN_MIN = np.array([60 - sensitivity, 50, 50], np.uint8)
    GREEN_MAX = np.array([60 + sensitivity, 255, 255], np.uint8)

    frame_threshed = cv.inRange(frame_hsv, GREEN_MIN, GREEN_MAX)

    top_left_row, top_left_col = np.nonzero(frame_threshed[0:(frame_r//2), 0:(frame_c//2)])
    top_right_row, top_right_col = np.nonzero(frame_threshed[0:(frame_r//2), (frame_c//2):frame_c])
    bottom_left_row, bottom_left_col = np.nonzero(frame_threshed[(frame_r//2):frame_r, 0:(frame_c//2)])
    bottom_right_row, bottom_right_col = np.nonzero(frame_threshed[(frame_r//2):frame_r, (frame_c//2):frame_c])

    top_left = (np.mean(top_left_col), np.mean(top_left_row))
    top_right = (np.mean(top_right_col) + frame_c//2, np.mean(top_right_row))
    bottom_left = (np.mean(bottom_left_col), np.mean(bottom_left_row) + frame_r//2)
    bottom_right = (np.mean(bottom_right_col) + frame_c//2, np.mean(bottom_right_row) + frame_r//2)

    paper_corners = [bottom_left, bottom_right, top_left, top_right]
    return frame_threshed, paper_corners

def pen_tracking(frame):
    frame_hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)

    # tunable params
    # BLUE_MIN = np.array([100, 100, 0], np.uint8)
    # BLUE_MAX = np.array([140, 200, 200], np.uint8)
    # frame_threshed = cv.inRange(frame_hsv, BLUE_MIN, BLUE_MAX)

    sensitivity = 15
    GREEN_MIN = np.array([60 - sensitivity, 50, 50], np.uint8)
    GREEN_MAX = np.array([60 + sensitivity, 255, 255], np.uint8)
    frame_threshed = cv.inRange(frame_hsv, GREEN_MIN, GREEN_MAX)

    pen_rows, pen_cols = np.nonzero(frame_threshed)
    cv.imshow("pen", frame_threshed)
    if len(pen_rows) == 0 or len(pen_cols) == 0:
        return 0,0
    pen_coords = (int(np.mean(pen_cols)), int(np.mean(pen_rows)))
    return pen_coords

def get_projection_matrix(paper_corners):
    """
    paper_corners points of the paper: 
        top-left, top-right, bottom-left, bottom-right
    The pen on camera:
        bottom-left, bottom-right, top-left, top-right
    """
    
    canvas_corners = np.array([[0,0], [canvasW-1,0], [0, canvasH-1], [canvasW-1, canvasH-1]])

    # A is the linear equations to be solved
    A = np.empty((8,9))
    for pt in range(4):
        x_l, y_l = paper_corners[pt]
        x_r, y_r = canvas_corners[pt]
        # each point corresponds to 2 rows of the matrix A
        A[pt*2] = [  0,   0, 0, x_l, y_l, 1, -y_r * x_l, -y_r * y_l, -y_r]
        A[pt*2 + 1] = [x_l, y_l, 1,   0,   0, 0, -x_r * x_l, -x_r * y_l, -x_r]
    U, s, V = np.linalg.svd(A)
    H = V[-1].reshape(3,3) # homography matrix

    return H

def paper_to_canvas(H, paper_point):
    x, y = paper_point
    canvas_pt = H @ np.array([x,y,1]).T
    scale = 1 / canvas_pt[2]
    canvas_pt *= scale

    return int(canvas_pt[0]), int(canvas_pt[1])

def calibration():
    video = cv.VideoCapture(0)
    # calibration_out = cv.VideoWriter('calibration.mp4',cv.VideoWriter_fourcc(*'mp4v'), 15, (1280,720))
    while video.isOpened():
        isTrue, frame = video.read()
        if not isTrue: break
        # Flip the frame for mirrored image
        frame = cv.flip(frame,1)
        frame_threshed, paper_corners = find_paper_corners(frame)
        font = cv.FONT_HERSHEY_SIMPLEX
        font_scale = 1
        color = (255,0,0)
        thickness = 2
        for pt in paper_corners:
            try:
                pt = int(pt[0]), int(pt[1])
                cv.putText(frame, str(pt), pt ,font, font_scale, color, thickness, cv.LINE_AA)
            except:
                pass
    
        # calibration_out.write(frame)

        cv.imshow("paper_corner", frame_threshed)
        cv.imshow("Calibration", frame)
        if cv.waitKey(20) & 0xFF==ord('q'):
            break 
    H = get_projection_matrix(paper_corners)
    video.release()
    # calibration_out.release()
    cv.destroyAllWindows()
    return H

def draw_loop(H):
    # Read input video
    video = cv.VideoCapture(0)
    # canvas_out = cv.VideoWriter('canvas.mp4',cv.VideoWriter_fourcc(*'mp4v'), 15, (canvasW,canvasH))
    # video_out = cv.VideoWriter('draw_capture.mp4',cv.VideoWriter_fourcc(*'mp4v'), 15, (1280,720))

    # Tracker and bounding box initialization
    # tracker = cv.TrackerCSRT_create()
    # for i in range(10):
    #     isTrue, frame = video.read()
    isTrue, frame = video.read()
    frame = cv.flip(frame,1)

    # initialize object tracker for the pen
    # bbox = cv.selectROI(frame, False)
    # ok = tracker.init(frame, bbox)
    canvas = np.uint8(np.ones((canvasH,canvasW,3)) * 255)
    # (x,y,w,h) =[int(v) for v in bbox]
    # penX, penY = x + w//2, y + h//2
    penX,penY = pen_tracking(frame)
    x1, y1 = paper_to_canvas(H, (penX,penY))
    # Read each frame
    while video.isOpened():

        isTrue, frame = video.read()
        if not isTrue: break

        # Flip the frame for mirroed image
        frame = cv.flip(frame,1)
        # h,w,_ = frame.shape

        # update object tracker
        # ok,bbox=tracker.update(frame)
        # if ok:
        #     (x,y,w,h)=[int(v) for v in bbox]
        #     cv.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)

        # penX, penY = x + w//2, y + h//2
        penX,penY = pen_tracking(frame)
        x2, y2 = paper_to_canvas(H, (penX,penY))
        if 0 <= x1 < canvasW and 0 <= y1 < canvasH and 0 <= x2 < canvasW and 0 <= y2 < canvasH:
            if penX == 0 and penY == 0:
                x1,y1= x2,y2
            else:
                canvas = cv.line(canvas, (x1,y1),(x2,y2), [255,0,0], 5)
        
        x1,y1 = x2,y2

        key_press = cv.waitKey(20)
        # press q to quit
        if key_press & 0xFF==ord('q'):
            break 
        # press c to clear
        elif key_press & 0xFF==ord('c'):
            canvas = np.uint8(np.ones((canvasH,canvasW,3)) * 255)

        # show the video frames
        # video_out.write(frame)
        # canvas_out.write(canvas)
        
        cv.imshow('Tracking Video', frame)
        cv.imshow("Canvas", canvas)

    video.release()
    # video_out.release()
    # canvas_out.release()
    cv.destroyAllWindows()

if __name__ == "__main__":
    H = calibration()
    draw_loop(H)