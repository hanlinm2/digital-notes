# Digital Notes With Any Pen on Any Surface

Watch the video demo:

[![Video Demo](https://img.youtube.com/vi/yeBxkBcJyF4/0.jpg)](https://www.youtube.com/watch?v=yeBxkBcJyF4)

## Requirements

```
OpenCV
numpy
```

## To run the program:

```
python3 pen_tracking.py
```

## Instructions
1. Use green masking tape on the corners of an A4 paper 

<img src="img/tape.jpeg" alt="tape" width="300"/>
<img src="img/paper.jpeg" alt="taped paper" width="300"/>

2. Tape the tip of a pen or pencil with the same masking tape.
3. Run ```python3 pen_tracking.py```
4. Calibrate. Close the laptop lid so that the four corners of the paper is in view and have coordinates attached to them. Press Q to confirm calibration and move the paper away.

<img src="img/calibration.jpeg" alt="paper with corner coordinates on camera" width="300"/>

5. Now you can start drawing and taking notes. Press Q to quit. Press C to clear canvas.

<img src="img/canvas.jpeg" alt="white background with writing on it in blue" width="300"/>
<img src="img/draw.jpeg" alt="Drawing with taped pen on table from the camera view" width="300"/>